import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import NotFoundPage from "../pages/NotFoundPage/NotFoundPage";
import HomePage from "../pages/HomePage/HomePage";
import UserPage from "../pages/UserPage/UserPage";
import RightPage from "../pages/RightPage/RightPage";
import DomainPage from "../pages/DomainPage/DomainPage";
import UserRightPage from "../pages/RightPage/UserRightPage";

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route path='/' exact component={HomePage} />
            <Route path='/users' exact component={UserPage} />
            <Route path='/rights/get/:id' exact component={UserRightPage} />
            <Route path='/domains' exact component={DomainPage} />
            <Route path='/rights' exact component={RightPage} />
            <Route component={NotFoundPage} />
        </Switch>
    </BrowserRouter>
)

export default Routes