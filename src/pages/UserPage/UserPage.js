import React from 'react'
import UserList from "../../common/MainContainer/components/Users/UserList";
import {Container} from "@material-ui/core";
import AddUserDialog from "../../common/MainContainer/components/Users/components/AddUserDialog";
import Navbar from "../../common/MainContainer/components/Navbar/Navbar";

const UserPage = () => (
    <Container>
        <Navbar/>
        <UserList/>
        <AddUserDialog/>
    </Container>
)

export default UserPage