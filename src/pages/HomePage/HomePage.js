import React from 'react'
import Navbar from "../../common/MainContainer/components/Navbar/Navbar";
import {Container} from "@material-ui/core";

const HomePage = () => (
    <Container>
        <Navbar/>
        <h1>Welcome to IAM service</h1>
    </Container>
)

export default HomePage