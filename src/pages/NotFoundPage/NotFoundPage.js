import React from 'react'
import './NotFoundPage.css'

const Error = () => (
  <div>
    <div className='error-page'>
      <p> Not found page error 404 </p>
      <p>Oooops... something is wrong...</p>
    </div>
  </div>
)

export default Error
