import React from 'react'
import {Container} from "@material-ui/core";
import RightList from "../../common/MainContainer/components/Rights/RightList";
import Navbar from "../../common/MainContainer/components/Navbar/Navbar";

const RightPage = () => (
    <Container>
        <Navbar/>
        <RightList/>
    </Container>
)

export default RightPage