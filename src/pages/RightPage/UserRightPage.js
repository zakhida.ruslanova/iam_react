import React from 'react'
import {Container} from "@material-ui/core";
import Navbar from "../../common/MainContainer/components/Navbar/Navbar";
import AddRightDialog from "../../common/MainContainer/components/Rights/components/AddRightDialog";
import RightListForUser from "../../common/MainContainer/components/Rights/RightListForUser";

const RightPage = ({ match }) => (
    <Container>
        <Navbar/>
        <RightListForUser id={match.params.id}/>
        <AddRightDialog id={match.params.id}/>
    </Container>
)

export default RightPage