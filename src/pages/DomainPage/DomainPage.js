import React from 'react'
import Navbar from "../../common/MainContainer/components/Navbar/Navbar";
import {Container} from "@material-ui/core";
import DomainsList from "../../common/MainContainer/components/Domains/DomainsList";
import AddDomainDialog from "../../common/MainContainer/components/Domains/components/AddDomainDialog";

const DomainPage = () => (
    <Container>
        <Navbar/>
        <DomainsList/>
        <AddDomainDialog/>
    </Container>
)

export default DomainPage