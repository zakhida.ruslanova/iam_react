import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";
import SearchIcon from '@material-ui/icons/Search';
import {Link} from "react-router-dom";
import HomeIcon from '@material-ui/icons/Home';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginBottom: '10px'
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function Navbar() {
    const classes = useStyles();
    return (
        <AppBar position="static" className={classes.root}>
            <Toolbar>
                <Button component={Link} to="/">
                    <HomeIcon/>
                </Button>
                <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                    <MenuIcon/>
                </IconButton>
                <Button component={Link} to="/users">
                    <SearchIcon/>
                    Users
                </Button>
                <Button component={Link} to="/domains">
                    <SearchIcon/>
                    Domains
                </Button>
                <Button component={Link} to="/rights">
                    <SearchIcon/>
                    Rights
                </Button>
            </Toolbar>
        </AppBar>
    );
}