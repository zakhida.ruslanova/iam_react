import React from 'react'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {Container} from "@material-ui/core";
import UpdateRightDialog from "./components/UpdateRightDialog";
import {LOCALHOST_8080} from "../../../../constants/constants";
import DeleteRightForUser from "./components/DeleteRightForUser";

export default class RightListForUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rights: []
        }
    }

    componentDidMount() {
        window.userId = this.props.id; /*global variable for rights page*/
        fetch(LOCALHOST_8080 + "rights/get/" + this.props.id)
            .then(response => response.json())
            .then(data => this.setState({ rights: data }));
    }

    render() {
        return(
            <Container>
                <List component="nav" aria-label="secondary mailbox folders">
                    {this.state.rights.map(right =>
                        <ListItem button>
                            <DeleteRightForUser children={right.id} />
                            <UpdateRightDialog children={right.id}/>
                            <ListItemText key={right.id} text={right.rightname}/>
                            Right: {right.rightname} <br/> Domain: {right.domain}
                        </ListItem>)}
                </List>
            </Container>
        )
    }
}