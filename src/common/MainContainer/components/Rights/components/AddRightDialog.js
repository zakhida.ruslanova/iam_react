import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {LOCALHOST_3000, LOCALHOST_8080} from "../../../../../constants/constants";

export default class AddRightDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            username: '',
            right: '',
            domain: ''
        }
    }

    handleClickOpen = () => {
        this.setState({open:true});
    };

    handleClose = () => {
        this.setState({open:false});
    };

    handleChange = event => {
        this.setState({
            username: event.target.value,
            right: event.target.value,
            domain: event.target.value
        });
    }

    handleSubmit = event => {

        event.preventDefault();

        const right = {
            username: document.getElementById("username").value,
            right: document.getElementById("right").value,
            domain: document.getElementById("domain").value
        };

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({username: right.username, right: right.right, domain: right.domain})
        };

        fetch(LOCALHOST_8080 + 'users/addrighttouser', requestOptions)
            .then(response => response.json())
            .then(window.location.replace(LOCALHOST_3000 + 'rights/get/' + this.props.id));
    }

    render() {
        return(
            <div>
                <Button variant="outlined" color="primary" onClick={this.handleClickOpen}>
                    Add right to user
                </Button>
                <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Add right to user</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Here you can add right to user
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="username"
                            label="username"
                            type="username"
                            fullWidth
                            onChange={this.handleChange}
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            id="right"
                            label="right"
                            type="right"
                            fullWidth
                            onChange={this.handleChange}
                        />
                        <TextField
                            autoFocus
                            margin="dense"
                            id="domain"
                            label="domain"
                            type="domain"
                            fullWidth
                            onChange={this.handleChange}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="secondary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleSubmit} color="primary">
                            Create
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}