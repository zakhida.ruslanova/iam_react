import React from 'react';
import Button from '@material-ui/core/Button';
import {LOCALHOST_3000, LOCALHOST_8080} from "../../../../../constants/constants";

export default class DeleteRightForUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    handleDelete = event => {

        event.preventDefault();

        const requestOptions = {method: 'DELETE'};

        fetch(LOCALHOST_8080 + 'rights/delete/' + window.userId + '/' + this.props.children, requestOptions)
            .then(window.location.replace(LOCALHOST_3000 + 'rights/get/' + window.userId))
    }

    render() {
        return(
            <div>
                <Button variant="outlined" color="secondary" onClick={this.handleDelete}>Delete right</Button>
            </div>
        );
    }
}