import React from 'react'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {Container} from "@material-ui/core";
import {LOCALHOST_8080} from "../../../../constants/constants";

export default class RightList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rights: []
        }
    }

    componentDidMount() {
        fetch(LOCALHOST_8080 + 'rights')
            .then(response => response.json())
            .then(data => this.setState({ rights: data }));
    }

    render() {
        return(
            <Container>
                <List component="nav" aria-label="secondary mailbox folders">
                    {this.state.rights.map(right =>
                        <ListItem key={right.id} button>
                            <ListItemText  text={right.rightname}/>
                            Right: {right.rightname} <br/> Domain: {right.domain}
                        </ListItem>)}
                </List>
            </Container>
        )
    }
}