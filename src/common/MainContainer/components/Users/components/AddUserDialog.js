import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {LOCALHOST_3000, LOCALHOST_8080} from "../../../../../constants/constants";

export default class AddUserDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            username: '',
            password: ''
        }
    }

    handleClickOpen = () => {
        this.setState({open:true});
    };

    handleClose = () => {
        this.setState({open:false});
    };

    handleChange = event => {
        this.setState({
            username: event.target.value,
            password: event.target.value
        });
    }

    handleSubmit = event => {

        event.preventDefault();

        const user = {
            username: document.getElementById("username").value,
            password: document.getElementById("password").value
        };

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({username: user.username, password: user.password })
        };

        fetch(LOCALHOST_8080 + 'users/add', requestOptions)
            .then(response => response.json())
            .then(window.location.replace(LOCALHOST_3000 + 'users'));
    }

    render() {
        return(
            <div>
            <Button variant="outlined" color="primary" onClick={this.handleClickOpen}>
                Create user
            </Button>
            <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Create user</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Here you can create user
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="username"
                        label="username"
                        type="username"
                        fullWidth
                        onChange={this.handleChange}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        id="password"
                        label="password"
                        type="password"
                        fullWidth
                        onChange={this.handleChange}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose} color="secondary">
                        Cancel
                    </Button>
                    <Button onClick={this.handleSubmit} color="primary">
                        Create
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
        );
    }
}