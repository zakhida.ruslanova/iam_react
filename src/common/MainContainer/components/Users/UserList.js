import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {Box, Container, InputBase, Paper} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import UpdateUserDialog from "./components/UpdateUserDialog";
import Button from "@material-ui/core/Button";
import {LOCALHOST_3000, LOCALHOST_8080} from "../../../../constants/constants";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 1500,
        backgroundColor: theme.palette.background.paper,
    },
}));

class UserList extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            filterText: ''
        }
    }

    componentDidMount() {
        fetch(LOCALHOST_8080 + "users")
            .then(response => response.json())
            .then(data => this.setState({ users: data }));
    }

    handleChangeFind = event => {
        this.setState({filterText: event.target.value});
    }

    render() {
        const classes = this.props.classes;
        return(
            <Container className={classes.root}>
                <Box style={{marginBottom: '10px', display: 'flow-root'}}>
                    <Paper component="form" className={classes.root} style={{float: 'right'}}>
                        <IconButton type="submit" className={classes.iconButton} aria-label="search">
                            <SearchIcon/>
                        </IconButton>
                        <InputBase
                            className={classes.input}
                            placeholder="Sök efter namn"
                            inputProps={{'aria-label': 'Sök namn'}}
                            onChange={this.handleChangeFind}
                        />
                    </Paper>
                </Box>
                    <List component="nav" aria-label="secondary mailbox folders">
                        {this.state.users.filter(name => {
                           return name.username.toLowerCase().indexOf(this.state.filterText.toLowerCase()) >= 0
                        }).map(user =>
                        <ListItem key={user.id} button>
                            <Button component="button"
                                    color="primary"
                                    onClick={() => {
                                        window.location.replace(LOCALHOST_3000 + 'rights/get/' + `${user.id}`);
                                    }}>
                                Rights
                            </Button>
                            <UpdateUserDialog children={user.id}/>
                            <ListItemText text={user.username}/>{user.username}
                        </ListItem>)}
                    </List>
            </Container>
        )
    }
}

export default () => {
    const classes = useStyles();
    return (
        <UserList classes={classes} />
    )
}