import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {Container} from "@material-ui/core";
import {LOCALHOST_8080} from "../../../../constants/constants";
import DeleteDomain from "./components/DeleteDomain";
import UpdateDomainDialog from "./components/UpdateDomainDialog";
import AddRightToDomainDialog from "./components/AddRightToDomainDialog";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 1500,
        backgroundColor: theme.palette.background.paper,
    },
}));

class DomainsList extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            domains: []
        }
    }

    componentDidMount() {
        fetch(LOCALHOST_8080 + "domains")
            .then(response => response.json())
            .then(data => this.setState({ domains: data }));
    }

    render() {
        const classes = this.props.classes;
        return(
            <Container className={classes.root}>
                <List component="nav" aria-label="secondary mailbox folders">
                    {this.state.domains.map(domain =>
                        <ListItem key={domain.id} button>
                            <AddRightToDomainDialog children={domain.id}/>
                            <DeleteDomain children={domain.id}/>
                            <UpdateDomainDialog children={domain.id}/>
                            <ListItemText  text={domain.domain}/>{domain.domain}
                            <List component="nav" aria-label="secondary mailbox folders">
                                {domain.rights.map(right =>
                                    <ListItem key={right.id}  button>
                                        <ListItemText text={right}/>{right}
                                    </ListItem>)}
                                </List>
                        </ListItem>)}
                </List>
            </Container>
        )
    }
}

export default () => {
    const classes = useStyles();
    return (
        <DomainsList classes={classes} />
    )
}