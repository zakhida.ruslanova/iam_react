import React from 'react';
import Button from '@material-ui/core/Button';
import {LOCALHOST_3000, LOCALHOST_8080} from "../../../../../constants/constants";

export default class DeleteDomain extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    handleDelete = event => {

        event.preventDefault();

        const requestOptions = {method: 'DELETE'};

        fetch(LOCALHOST_8080 + 'domains/delete/' + this.props.children, requestOptions)
            .then(window.location.replace(LOCALHOST_3000 + 'domains'))
    }

    render() {
        return(
            <div>
                <Button variant="outlined" color="secondary" onClick={this.handleDelete}>Delete domain</Button>
            </div>
        );
    }
}