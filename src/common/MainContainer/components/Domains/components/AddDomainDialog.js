import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {LOCALHOST_3000, LOCALHOST_8080} from "../../../../../constants/constants";

export default class AddDomainDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            domain: ''
        }
    }

    handleClickOpen = () => {
        this.setState({open:true});
    };

    handleClose = () => {
        this.setState({open:false});
    };

    handleChange = event => {
        this.setState({
            domain: event.target.value
        });
    }

    handleSubmit = event => {

        event.preventDefault();

        const domain = {
            domain: document.getElementById("domain").value
        };

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({domain: domain.domain})
        };

        fetch(LOCALHOST_8080 + 'domains/add', requestOptions)
            .then(response => response.json())
            .then(window.location.replace(LOCALHOST_3000 + 'domains'));
    }

    render() {
        return(
            <div>
                <Button variant="outlined" color="primary" onClick={this.handleClickOpen}>
                    Create domain
                </Button>
                <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Create domain</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Here you can create domain
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="domain"
                            label="domain"
                            type="domain"
                            fullWidth
                            onChange={this.handleChange}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="secondary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleSubmit} color="primary">
                            Create
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}