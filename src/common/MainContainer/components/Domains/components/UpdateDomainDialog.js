import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {LOCALHOST_3000, LOCALHOST_8080} from "../../../../../constants/constants";

export default class UpdateDomainDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            domain: ''
        }
    }

    handleClickOpen = () => {
        this.setState({open:true});
    };

    handleClose = () => {
        this.setState({open:false});
    };

    handleChange = event => {
        this.setState({domain: event.target.value});
    }

    handleSubmit = event => {

        event.preventDefault();

        const domain = {
            domain: document.getElementById("domain").value
        };

        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({domain: domain.domain})
        };

        fetch(LOCALHOST_8080 + 'domains/update/' + this.props.children, requestOptions)
            .then(response => response.json())
            .then(window.location.replace(LOCALHOST_3000 + 'domains'));
    }

    render() {
        return(
            <div>
                <Button variant="outlined" color="primary" onClick={this.handleClickOpen}>
                    Update domain
                </Button>
                <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Update domain</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Here you can update domain
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="domain"
                            label="domain"
                            type="domain"
                            fullWidth
                            onChange={this.handleChange}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="secondary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleSubmit} color="primary">
                            Update
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}