import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {LOCALHOST_3000, LOCALHOST_8080} from "../../../../../constants/constants";

export default class AddRightToDomainDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            rightname: ''
        }
    }

    handleClickOpen = () => {
        this.setState({open:true});
    };

    handleClose = () => {
        this.setState({open:false});
    };

    handleChange = event => {
        this.setState({rightname: event.target.value});
    }

    handleSubmit = event => {

        event.preventDefault();

        const rightname = {rightname: document.getElementById("rightname").value};

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' }
        };

        fetch(LOCALHOST_8080 + 'domains/addrighttodomain/' + this.props.children + '?rightname=' + this.state.rightname, requestOptions)
            .then(response => response.json())
            .then(window.location.replace(LOCALHOST_3000 + 'domains'));
    }

    render() {
        return(
            <div>
                <Button variant="outlined" color="primary" onClick={this.handleClickOpen}>
                    Add right to domain
                </Button>
                <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Add right to domain</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Here you can add right to domain
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="rightname"
                            label="rightname"
                            type="rightname"
                            fullWidth
                            onChange={this.handleChange}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="secondary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleSubmit} color="primary">
                            Add right
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}